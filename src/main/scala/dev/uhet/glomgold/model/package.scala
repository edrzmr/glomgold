package dev.uhet.glomgold

package object model {

  case class LoadBalancerName(name: String) extends AnyVal
  case class AutoScalingGroupName(name: String) extends AnyVal
  case class TargetGroupArn(arn: String) extends AnyVal
  case class TargetGroupName(name: String) extends AnyVal

  case class InstanceId(id: String) extends AnyVal
  case object InstanceId { val empty = InstanceId("none") }
  case class SirId(id: String) extends AnyVal
  case class SirStatus(status: String) extends AnyVal

  object spot {
    sealed trait State
    case object Open extends State
    case object Active extends State
    case object Cancelled extends State
    case object Failed extends State
    case object Closed extends State
    case object Unknown extends State
  }
  
  object instance {
    sealed trait State
    case object Pending extends State
    case object Running extends State
    case object ShuttingDown extends State
    case object Terminated extends State
    case object Stopping extends State
    case object Stopped extends State
    case object Unknown extends State
  }
}
