package dev.uhet.glomgold

import dev.uhet.glomgold.model.{InstanceId, SirId}
import software.amazon.awssdk.services.ec2.model.SpotInstanceRequest

package object exception {

  case class EC2CloneException(instanceId: InstanceId,
                               sirId: SirId,
                               sir: SpotInstanceRequest)
    extends Exception(sir.status.message)
}
