package dev.uhet.glomgold.extension.model

import dev.uhet.glomgold.model.InstanceId
import dev.uhet.glomgold.model.TargetGroupArn
import dev.uhet.glomgold.model.TargetGroupName
import software.amazon.awssdk.services.elasticloadbalancingv2.model.TargetGroup
import software.amazon.awssdk.services.elasticloadbalancingv2.model.TargetHealthStateEnum

trait TargetGroupModelExtension {

  import software.amazon.awssdk.services.elasticloadbalancingv2.model.TargetHealthDescription

  implicit class TargetGroupIterableExtension(collection: Iterable[TargetGroup]) {
    lazy val arnSet: Set[TargetGroupArn] = collection.map(tg => TargetGroupArn(tg.targetGroupArn)).toSet
  }

  implicit class TargetGroupExtension(tg: TargetGroup) {
    lazy val arn: TargetGroupArn = TargetGroupArn(tg.targetGroupArn)
  }

  implicit class TargetHealthDescriptionIterableExtension(collection: Iterable[TargetHealthDescription]) {
    lazy val notHealthy: Boolean = collection.exists(thd => thd.targetHealth.state != TargetHealthStateEnum.HEALTHY)
    lazy val health: Iterable[(InstanceId, TargetHealthStateEnum)] = collection.map(thd => thd.id -> thd.health)
  }

  implicit class TargetHealthDescriptionExtension(thd: TargetHealthDescription) {
    lazy val id: InstanceId = InstanceId(thd.target.id)
    lazy val health: TargetHealthStateEnum = thd.targetHealth.state
  }

  implicit class TargetGroupMapExtension(tgMap: Map[TargetGroupName, TargetGroup]) {
    lazy val arnSet: Set[TargetGroupArn] = tgMap.values.arnSet
  }
  
}
