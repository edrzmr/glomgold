package dev.uhet.glomgold.extension.model

trait ModelExtension
  extends AutoScalingGroupModelExtension
    with InstanceModelExtension
    with InstanceStateNameModelExtension
    with SpotInstanceStateModelExtension
    with SpotInstanceRequestModelExtension
    with TargetGroupModelExtension
   
