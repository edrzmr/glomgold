package dev.uhet.glomgold.extension.model

import dev.uhet.glomgold.model.SirId
import software.amazon.awssdk.services.ec2.model.SpotInstanceRequest

trait SpotInstanceRequestModelExtension {
  
  implicit class SpotInstanceRequestModelExtension(sir: SpotInstanceRequest) {
    lazy val id: SirId = SirId(sir.spotInstanceRequestId)
  }
}
