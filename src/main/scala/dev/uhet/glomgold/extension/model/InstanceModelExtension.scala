package dev.uhet.glomgold.extension.model

import dev.uhet.glomgold.model.InstanceId
import software.amazon.awssdk.services.ec2.model.Instance
import software.amazon.awssdk.services.ec2.model.InstanceLifecycleType
import software.amazon.awssdk.services.ec2.model.SpotPlacement

import scala.collection.JavaConverters._

trait InstanceModelExtension {

  implicit class InstanceExtension(instance: Instance) {
    lazy val az: String = instance.placement.availabilityZone
    lazy val id: InstanceId = InstanceId(instance.instanceId)
    lazy val spotPlacement: SpotPlacement = SpotPlacement.builder.availabilityZone(instance.az).build
    lazy val securityGroupIds: List[String] = instance.securityGroups.asScala.toList.map(_.groupId)
  }

  implicit class InstanceIterableExtension(collection: Iterable[Instance]) {
    lazy val id: Iterable[InstanceId] = collection.map(instance => instance.id)
    lazy val spot: Iterable[Instance] = collection
      .filter(instance => instance.instanceLifecycle == InstanceLifecycleType.SPOT)
    lazy val onDemand: Iterable[Instance] = collection
      .filterNot(instance => instance.instanceLifecycle == InstanceLifecycleType.SPOT)

    def tagValue(key: String): Iterable[String] = collection
      .foldLeft(Set.empty[String]) {
        case (acc, instance) =>
          instance.tags.asScala
            .map(tag => tag.key -> tag.value).toMap
            .get(key) match {
            case None => acc
            case Some(value) => acc + value
          }
      }
  }

}
