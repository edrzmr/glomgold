package dev.uhet.glomgold

import dev.uhet.glomgold.extension.model.AutoScalingGroupModelExtension
import dev.uhet.glomgold.extension.model.InstanceModelExtension
import dev.uhet.glomgold.extension.model.InstanceStateNameModelExtension
import dev.uhet.glomgold.extension.model.SpotInstanceRequestModelExtension
import dev.uhet.glomgold.extension.model.SpotInstanceStateModelExtension
import dev.uhet.glomgold.extension.model.TargetGroupModelExtension

package object extension {

  trait Model extends AutoScalingGroupModelExtension
    with InstanceModelExtension
    with InstanceStateNameModelExtension
    with SpotInstanceStateModelExtension
    with SpotInstanceRequestModelExtension
    with TargetGroupModelExtension

}
