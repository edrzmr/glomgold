package dev.uhet.glomgold

import dev.uhet.glomgold.client.{ASGClient, EC2Client, ELBv2Client}
import dev.uhet.glomgold.extension.model.ModelExtension
import dev.uhet.glomgold.model._
import software.amazon.awssdk.services.autoscaling.model.AutoScalingGroup
import software.amazon.awssdk.services.ec2.model.Instance
import software.amazon.awssdk.services.elasticloadbalancingv2.model.{LoadBalancer, TargetGroup, TargetHealthDescription}

import scala.util.{Failure, Success, Try}

case class Database(asgMap: Map[AutoScalingGroupName, AutoScalingGroup],
                    lbMap: Map[LoadBalancerName, LoadBalancer],
                    tgMapGrouped: Map[LoadBalancerName, Map[TargetGroupName, TargetGroup]],
                    tgMap: Map[TargetGroupName, TargetGroup],
                    thdMap: Map[TargetGroupName, Set[TargetHealthDescription]],
                    tgInstanceMap: Map[TargetGroupName, Set[Instance]]) extends ModelExtension {

  lazy val enabledAsgMap: Map[AutoScalingGroupName, AutoScalingGroup] =
    asgMap.filter { case (_, asg) => asg.desiredCapacity > 0 && asg.maxSize > 0 && asg.minSize > 0 }
  
  lazy val index: Map[LoadBalancerName, Map[AutoScalingGroupName, Set[TargetGroupName]]] = {
    tgMapGrouped.map {
      case (lbName, tgInnerMap) => lbName -> join(asgMap, tgInnerMap)
    }
  }
  
  lazy val tgArnMap: Map[TargetGroupArn, TargetGroupName] =
    tgMap.map { case (tgName, tg) => tg.arn -> tgName }

  private def join(asgMap: Map[AutoScalingGroupName, AutoScalingGroup],
                   tgMap: Map[TargetGroupName, TargetGroup]): Map[AutoScalingGroupName, Set[TargetGroupName]] = {

    asgMap.foldLeft(Map.empty[AutoScalingGroupName, Set[TargetGroupName]]) {
      case (acc, (asgName, asg)) =>
        val intersected = asg.targetGroupArnSet.intersect(tgMap.arnSet)
        if (intersected.isEmpty) acc
        else acc + (asgName -> intersected.map(tgArnMap))
    }
  }

}

object Database {

  def load(lbNameSet: Set[LoadBalancerName]): Try[Database] =
    ASGClient.asgMap.flatMap(asgMap => load(lbNameSet, asgMap))

  private def load(lbNameSet: Set[LoadBalancerName],
                   asgMap: Map[AutoScalingGroupName, AutoScalingGroup]): Try[Database] =

    ELBv2Client.lbMap(lbNameSet).flatMap(lbMap => load(asgMap, lbMap))

  private def load(asgMap: Map[AutoScalingGroupName, AutoScalingGroup],
                   lbMap: Map[LoadBalancerName, LoadBalancer]): Try[Database] =

    Try(lbMap.map { case (lbName, lb) =>
      ELBv2Client.tgMap(lb) match {
        case Failure(exception) => throw exception
        case Success(tgMap) => lbName -> tgMap
      }
    }).flatMap(tgMapGrouped => load(asgMap, lbMap, tgMapGrouped))

  private def load(asgMap: Map[AutoScalingGroupName, AutoScalingGroup],
                   lbMap: Map[LoadBalancerName, LoadBalancer],
                   tgMapGrouped: Map[LoadBalancerName, Map[TargetGroupName, TargetGroup]]): Try[Database] = {

    val tgMap: Map[TargetGroupName, TargetGroup] =
      tgMapGrouped.foldLeft(Map.empty[TargetGroupName, TargetGroup]) {
        case (acc, (_, innerTgMap)) => acc ++ innerTgMap
      }

    load(asgMap, lbMap, tgMapGrouped, tgMap)
  }

  private def load(asgMap: Map[AutoScalingGroupName, AutoScalingGroup],
                   lbMap: Map[LoadBalancerName, LoadBalancer],
                   tgMapGrouped: Map[LoadBalancerName, Map[TargetGroupName, TargetGroup]],
                   tgMap: Map[TargetGroupName, TargetGroup]): Try[Database] =

    Try({
      val thdMap = tgMap.map { case (tgName, tg) =>
        ELBv2Client.thdSet(tg) match {
          case Failure(exception) => throw exception
          case Success(thdSet) => tgName -> thdSet
        }
      }

      val tgInstanceMap = thdMap.map { case (tgName, thdSet) =>
        val instanceIds = thdSet.map(thd => InstanceId(thd.target.id))
        EC2Client.instanceSet(instanceIds) match {
          case Failure(exception) => throw exception
          case Success(instanceSet) => tgName -> instanceSet
        }
      }

      (thdMap, tgInstanceMap)

    }).map { case (thdMap, tgInstanceMap) =>
      new Database(asgMap, lbMap, tgMapGrouped, tgMap, thdMap, tgInstanceMap)
    }


}
