package dev.uhet.glomgold.client

import dev.uhet.glomgold.exception.EC2CloneException
import dev.uhet.glomgold.extension.model.ModelExtension
import dev.uhet.glomgold.model.InstanceId
import dev.uhet.glomgold.model.SirId
import dev.uhet.glomgold.model.spot.Active
import software.amazon.awssdk.services.ec2.model._

import scala.collection.JavaConverters._
import scala.util.Failure
import scala.util.Success
import scala.util.Try

class EC2CloneClient(val instance: Instance,
                     val price: Double,
                     val maxWaitRetries: Int) extends Client with ModelExtension {

  import EC2CloneClient._

  private val runInstancesMonitoringEnabled =
    RunInstancesMonitoringEnabled.builder
      .enabled(!disabledMonitoringStates.contains(instance.monitoring.state))
      .build

  def run(): Try[(Instance, SpotInstanceRequest)] = Try({

    val userData = EC2Client.findInstanceUserData(instance) match {
      case Failure(exception) => throw exception
      case Success(ud) => ud
    }

    val sirClient = EC2Client.client
      .requestSpotInstances(buildSpotInstancesRequest(userData))
      .spotInstanceRequests
      .asScala.toList.headOption match {
      case None => throw new RuntimeException("missing spot instance request on request")
      case Some(sir) => EC2SirClient(instance, SirId(sir.spotInstanceRequestId))
    }

    val (spotInstance, spotInstanceRequest) = sirClient.waitForActiveState match {
      case Failure(exception) => throw exception
      case Success(sir) => sir.state.asScala match {
        case Active =>
          EC2Client.findInstance(InstanceId(sir.instanceId)) match {
            case Failure(exception) => throw exception
            case Success(si) => EC2Client.createTags(si, spotInstanceTags(sir)) match {
              case Failure(exception) => throw exception
              case Success(_) => (si, sir)
            }
          }
        case _ => sirClient.cancel match {
          case Failure(exception) => throw exception
          case Success(sirId) => throw EC2CloneException(instance.id, sirId, sir)
        }
      }
    }

    EC2Client.waitForState(InstanceId(spotInstance.instanceId)) match {
      case Failure(exception) => throw exception
      case Success(_) =>
    }

    (spotInstance, spotInstanceRequest)
  })

  private def spotInstanceTags(sir: SpotInstanceRequest): Map[String, String] = {
    instance.tags.asScala.toList
      .map(tag => tag.key -> tag.value)
      .filterNot(_._1.startsWith("aws:"))
      .toMap ++ Map(
      "GlomgoldOriginalInstanceId" -> instance.instanceId,
      "GlomgoldSpotInstanceRequestId" -> sir.spotInstanceRequestId,
    )
  }

  private def buildSpotInstancesRequest(userData: String) =
    RequestSpotInstancesRequest.builder
      .spotPrice(price.toString)
      .instanceCount(1)
      .`type`(SpotInstanceType.ONE_TIME)
      .launchSpecification(RequestSpotLaunchSpecification.builder
        .imageId(instance.imageId)
        .instanceType(instance.instanceType)
        .placement(instance.spotPlacement)
        .monitoring(runInstancesMonitoringEnabled)
        .securityGroupIds(instance.securityGroupIds.asJavaCollection)
        .subnetId(instance.subnetId())
        .userData(userData)
        .build)
      .build
}

object EC2CloneClient {
  val disabledMonitoringStates = List(MonitoringState.DISABLED, MonitoringState.DISABLED)

  def apply(instance: Instance, price: Double): EC2CloneClient = new EC2CloneClient(
    instance = instance,
    price = price,
    maxWaitRetries = 20
  )
}
