package dev.uhet.glomgold.client

import dev.uhet.glomgold.extension.model.ModelExtension
import dev.uhet.glomgold.model._
import software.amazon.awssdk.services.ec2.Ec2Client
import software.amazon.awssdk.services.ec2.model._
import software.amazon.awssdk.services.ec2.model.DescribeInstancesRequest

import scala.annotation.tailrec
import scala.collection.JavaConverters._
import scala.language.postfixOps
import scala.util.Failure
import scala.util.Success
import scala.util.Try


object EC2Client extends Client with ModelExtension {

  import dev.uhet.glomgold.model.InstanceId
  import dev.uhet.glomgold.model.SirId

  private[client] val client = Ec2Client.builder
    .httpClient(httpSyncClient)
    .build

  def findInstance(id: InstanceId): Try[Instance] =
    Try(findInstances(List(id)) match {
      case Failure(exception) => throw exception
      case Success(instances) => instances.head
    })

  def findInstances(instancesIds: List[InstanceId]): Try[List[Instance]] = {
    val request = DescribeInstancesRequest.builder
      .instanceIds(instancesIds.map(_.id).asJava)
      .build

    Try(client.describeInstances(request)
      .reservations
      .asScala
      .toList
      .flatMap(_.instances.asScala.toList))
  }

  def instanceSet(instancesIds: Set[InstanceId]): Try[Set[Instance]] = {
    val request = DescribeInstancesRequest.builder
      .instanceIds(instancesIds.map(_.id).asJava)
      .build

    Try(client.describeInstances(request))
      .map(resp => resp.reservations.asScala
        .flatMap(reservation => reservation.instances.asScala).toSet)
  }


  def findInstanceUserData(instance: Instance): Try[String] = findInstanceUserData(InstanceId(instance.instanceId))

  def findInstanceUserData(instanceId: InstanceId): Try[String] = {
    val request = DescribeInstanceAttributeRequest.builder
      .instanceId(instanceId.id)
      .attribute("userData")
      .build

    Try(client.describeInstanceAttribute(request)
      .userData()
      .value())
  }

  def describeSpotInstanceRequest(sirId: SirId): Try[SpotInstanceRequest] = {
    val request = DescribeSpotInstanceRequestsRequest.builder
      .spotInstanceRequestIds(List(sirId.id).asJavaCollection)
      .build

    Try(client.describeSpotInstanceRequests(request).spotInstanceRequests.asScala.toList.headOption match {
      case None => throw new RuntimeException("missing spot instance request on describe")
      case Some(spotInstanceRequest) => spotInstanceRequest
    })
  }

  def cancelSpotInstanceRequest(sirId: SirId): Try[SirId] = {
    val request = CancelSpotInstanceRequestsRequest.builder
      .spotInstanceRequestIds(sirId.id)
      .build

    client.cancelSpotInstanceRequests(request).cancelledSpotInstanceRequests().asScala.toList.headOption match {
      case None => throw new RuntimeException("missing spot instance request on cancel")
      case Some(cancelledSpotInstanceRequest) => cancelledSpotInstanceRequest.state match {
        case CancelSpotInstanceRequestState.CANCELLED => Success(sirId)
        case _ => throw new RuntimeException(s"something wrong with cancel of sir: ${sirId.id}")
      }
    }
  }

  def createTags(instance: Instance, tags: Map[String, String]): Try[CreateTagsResponse] = {
    val tagList = tags.map { case (key, value) => Tag.builder
      .key(key)
      .value(value)
      .build
    }

    val request = CreateTagsRequest.builder
      .tags(tagList.asJavaCollection)
      .resources(instance.instanceId)
      .build

    Try(client.createTags(request))
  }

  def waitForState(instanceId: InstanceId,
                   maxWaitRetries: Int = 20,
                   instanceState: instance.State = instance.Running): Try[instance.State] = {

    @tailrec
    def loop(attempts: Int, last: instance.State): instance.State = {

      waitTimeoutMs.foreach(timeout => Thread.sleep(timeout))

      if (attempts == 0) throw new Exception(s"could not research " +
        s"expected state: $instanceState, " +
        s"last state: $last, " +
        s"max retries: $maxWaitRetries")
      else describeInstanceState(instanceId) match {
        case Failure(_) => loop(attempts - 1, last)
        case Success(state) if state != instanceState => loop(attempts - 1, state)
        case Success(state) => state
      }
    }

    Try(loop(maxWaitRetries, instance.Unknown))
  }

  def describeInstanceState(instanceId: InstanceId): Try[instance.State] = {
    val request = DescribeInstanceStatusRequest.builder
      .instanceIds(instanceId.id)
      .build

    Try(client.describeInstanceStatus(request)
      .instanceStatuses
      .asScala
      .head
      .instanceState
      .name
      .asScala)
  }

}
