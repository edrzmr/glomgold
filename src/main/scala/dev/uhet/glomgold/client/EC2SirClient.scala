package dev.uhet.glomgold.client

import dev.uhet.glomgold.extension.model.ModelExtension
import dev.uhet.glomgold.model.SirId
import dev.uhet.glomgold.model.spot.Active
import software.amazon.awssdk.services.ec2.model.Instance
import software.amazon.awssdk.services.ec2.model.SpotInstanceRequest

import scala.annotation.tailrec
import scala.util.Failure
import scala.util.Success
import scala.util.Try

class EC2SirClient(val originalInstance: Instance,
                   val sirId: SirId,
                   val maxWaitRetries: Int) extends Client with ModelExtension {

  def waitForActiveState: Try[SpotInstanceRequest] = {

    @tailrec
    def loop(attempts: Int, last: Try[SpotInstanceRequest]): Try[SpotInstanceRequest] = {

      waitTimeoutMs.foreach(timeout => Thread.sleep(timeout))

      if (attempts == 0) last
      else EC2Client.describeSpotInstanceRequest(sirId) match {
        case failure@Failure(_) => loop(attempts - 1, failure)
        case success@Success(sir) => sir.state.asScala match {
          case Active => success
          case _ => loop(attempts - 1, success)
        }
      }
    }

    loop(maxWaitRetries, Failure(new Exception("some strange happened here")))
  }

  def cancel: Try[SirId] = EC2Client.cancelSpotInstanceRequest(sirId)


}

object EC2SirClient {
  def apply(instance: Instance, sirId: SirId): EC2SirClient = new EC2SirClient(
    originalInstance = instance,
    sirId = sirId,
    maxWaitRetries = 10
  )
}
