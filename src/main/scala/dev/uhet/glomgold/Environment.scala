package dev.uhet.glomgold

import dev.uhet.glomgold.model.LoadBalancerName

object Environment {

  val SPOT_MAX_PRICE: Double = get("GLOMGOLD_SPOT_MAX_PRICE").toDouble
  val ON_DEMAND_MIN_INSTANCES: Int = get("GLOMGOLD_ON_DEMAND_MIN_INSTANCES").toInt
  val LOAD_BALANCER_NAMES: Set[LoadBalancerName] = get("GLOMGOLD_LOAD_BALANCER_NAMES")
    .split(",")
    .map(lbName => LoadBalancerName(lbName.trim))
    .toSet

  private def get(name: String): String = sys.env.get(name) match {
    case Some(value) => value
    case None => throw new Exception(s"missing $name environment variable")
  }
}
