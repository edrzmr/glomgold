package dev.uhet.glomgold

import com.amazonaws.services.lambda.runtime.Context
import com.amazonaws.services.lambda.runtime.events.ScheduledEvent

class AWSLambda {

  def handleRequest(event: ScheduledEvent, context: Context): String = {
    println(s"lambda event: $event")

    Runner().run()

    "think i'm phony?"
  }
}
